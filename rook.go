package main


func getMovesRook(board [8][8]Piece, pos Position) []Position {
	var toPos []Position
	var rank, field int8
	rank, field = int8(pos.Rank) - 1, int8(pos.Field) - 1
	piece := board[rank][field]
	color := piece.GetColor()
	opp_color := color.GetOpponentColor()

	addPos := func (i, j int8) bool {
		p_color := board[i][j].GetColor()
		if p_color == Empty || p_color == opp_color {
			position := Position{
				Rank: uint8(i) + 1,
				Field: uint8(j) + 1,
			}
			toPos = append(toPos, position)
		}
		return p_color == Empty
	}

	var i, j int8
	for i = rank + 1; i < 8; i++ {
		if !addPos(i, field) {
			break
		}
	}
	for i = rank - 1; i >= 0; i-- {
		if !addPos(i, field) {
			break
		}
	}
	for j = field + 1; j < 8; j++ {
		if !addPos(rank, j) {
			break
		}
	}
	for j = field - 1; j >= 0; j-- {
		if !addPos(rank, j) {
			break
		}
	}
	return toPos
}
