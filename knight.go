package main


func getMovesKnight(board [8][8]Piece, pos Position) []Position {
	var toPos []Position
	var rank, field int8
	rank, field = int8(pos.Rank) - 1, int8(pos.Field) - 1

	color := board[rank][field].GetColor()

	addPos := func (i, j int8) {
		if i < 0 || i > 7 || j < 0 || j > 7 {
			return
		}
		if board[i][j].GetColor() == color {
			return
		}
		position := Position{
			Rank: uint8(i) + 1,
			Field: uint8(j) + 1,
		}
		toPos = append(toPos, position)
	}

	addPos(rank + 2, field + 1)
	addPos(rank + 2, field - 1)

	addPos(rank - 2, field + 1)
	addPos(rank - 2, field - 1)

	addPos(rank + 1, field + 2)
	addPos(rank + 1, field - 2)

	addPos(rank - 1, field + 2)
	addPos(rank - 1, field - 2)

	return toPos
}
