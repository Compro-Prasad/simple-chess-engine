package main

import "errors"
import "fmt"

type Position struct {
	Rank uint8
	Field uint8
}


func Pos(str string) Position {
	var pos Position
	pos.set(str)
	return pos
}


func (position *Position) set(strPos string) error {
	if strPos[0] < 'a' || strPos[0] > 'h' || strPos[1] < '1' || strPos[1] > '8' {
		return errors.New(fmt.Sprintf("Invalid string %v", strPos))
	}
	position.Field = strPos[0] - 97 + 1
	position.Rank = strPos[1] - 49 + 1
	return nil
}


func (position Position) String() string {
	return fmt.Sprintf("%v%v", string(position.Field + 96), position.Rank)
}


var (
	A1 = Pos("a1")
	B1 = Pos("b1")
	C1 = Pos("c1")
	D1 = Pos("d1")
	E1 = Pos("e1")
	F1 = Pos("f1")
	G1 = Pos("g1")
	H1 = Pos("h1")

	A2 = Pos("a2")
	B2 = Pos("b2")
	C2 = Pos("c2")
	D2 = Pos("d2")
	E2 = Pos("e2")
	F2 = Pos("f2")
	G2 = Pos("g2")
	H2 = Pos("h2")

	A3 = Pos("a3")
	B3 = Pos("b3")
	C3 = Pos("c3")
	D3 = Pos("d3")
	E3 = Pos("e3")
	F3 = Pos("f3")
	G3 = Pos("g3")
	H3 = Pos("h3")

	A4 = Pos("a4")
	B4 = Pos("b4")
	C4 = Pos("c4")
	D4 = Pos("d4")
	E4 = Pos("e4")
	F4 = Pos("f4")
	G4 = Pos("g4")
	H4 = Pos("h4")

	A5 = Pos("a5")
	B5 = Pos("b5")
	C5 = Pos("c5")
	D5 = Pos("d5")
	E5 = Pos("e5")
	F5 = Pos("f5")
	G5 = Pos("g5")
	H5 = Pos("h5")

	A6 = Pos("a6")
	B6 = Pos("b6")
	C6 = Pos("c6")
	D6 = Pos("d6")
	E6 = Pos("e6")
	F6 = Pos("f6")
	G6 = Pos("g6")
	H6 = Pos("h6")

	A7 = Pos("a7")
	B7 = Pos("b7")
	C7 = Pos("c7")
	D7 = Pos("d7")
	E7 = Pos("e7")
	F7 = Pos("f7")
	G7 = Pos("g7")
	H7 = Pos("h7")

	A8 = Pos("a8")
	B8 = Pos("b8")
	C8 = Pos("c8")
	D8 = Pos("d8")
	E8 = Pos("e8")
	F8 = Pos("f8")
	G8 = Pos("g8")
	H8 = Pos("h8")
)
