package main


func getMovesPawn(board [8][8]Piece, pos Position) []Position {
	var toPos []Position
	var rank, field int8
	rank, field = int8(pos.Rank) - 1, int8(pos.Field) - 1
	if rank == 0 || rank == 7 {
		panic("Pawn cannot be at rank 1 or 8")
	}

	color := board[rank][field].GetColor()
	opp_color := color.GetOpponentColor()

	addPos := func (i, j int8, corner bool) {
		if i < 0 || i > 7 || j < 0 || j > 7 {
			return
		}
		p_color := board[i][j].GetColor()
		if corner && p_color == color {
			return
		}
		if !corner && p_color == opp_color {
			return
		}
		position := Position{
			Rank: uint8(i) + 1,
			Field: uint8(j) + 1,
		}
		toPos = append(toPos, position)
	}

	nextRank := rank + 1
	nnextRank := rank + 2
	if color == Black {
		nextRank = rank - 1
		nnextRank = rank - 2
	}

	if color == Black && nnextRank == 4 || color == White && nnextRank == 3 {
		addPos(nnextRank, field, false)
	}

	addPos(nextRank, field - 1, true)
	addPos(nextRank, field, false)
	addPos(nextRank, field + 1, true)

	return toPos
}
