package main


func getMovesBishop(board [8][8]Piece, pos Position) []Position {
	var toPos []Position
	var rank, field int8
	rank, field = int8(pos.Rank) - 1, int8(pos.Field) - 1
	piece := board[rank][field]
	color := piece.GetColor()
	opp_color := color.GetOpponentColor()

	addPos := func (i, j int8) bool {
		p_color := board[i][j].GetColor()
		if p_color == Empty || p_color == opp_color {
			position := Position{
				Rank: uint8(i) + 1,
				Field: uint8(j) + 1,
			}
			toPos = append(toPos, position)
		}
		return p_color == Empty
	}

	var i, j int8
	for i, j = rank + 1, field + 1; i < 8 && j < 8; i, j = i + 1, j + 1 {
		if !addPos(i, j) {
			break
		}
	}
	for i, j = rank + 1, field - 1; i < 8 && j >= 0; i, j = i + 1, j - 1 {
		if !addPos(i, j) {
			break
		}
	}
	for i, j = rank - 1, field + 1; i >= 0 && j < 8; i, j = i - 1, j + 1 {
		if !addPos(i, j) {
			break
		}
	}
	for i, j = rank - 1, field - 1; i >= 0 && j >= 0; i, j = i - 1, j - 1 {
		if !addPos(i, j) {
			break
		}
	}
	return toPos
}
