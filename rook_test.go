package main

import (
	"testing"
)


func TestGetRookMoves(t *testing.T) {
	testGetMoves(
		t,
		[8][8]Piece{
			{__, __, __, __, __, __, __, __},
			{__, __, __, __, __, __, __, __},
			{__, __, __, __, __, __, __, __},
			{__, wR, __, wR, __, bB, __, __},
			{__, __, __, __, __, __, __, __},
			{__, __, __, __, __, bQ, __, __},
			{__, __, __, __, __, __, __, __},
			{__, __, __, bK, __, __, __, __},
		},
		D4,
		getMovesRook,
		[]Position{D5, D6, D7, D8, D3, D2, D1, E4, F4, C4},
	)
}
