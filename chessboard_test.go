package main

import (
	"reflect"
	"testing"
)


func TestChessboardInitialize(t *testing.T) {
	var board ChessBoard
	board.Initialize()

	if !reflect.DeepEqual(board.board, [8][8]Piece{
		{wR, wN, wB, wQ, wK, wB, wN, wR},
		{wP, wP, wP, wP, wP, wP, wP, wP},
		{__, __, __, __, __, __, __, __},
		{__, __, __, __, __, __, __, __},
		{__, __, __, __, __, __, __, __},
		{__, __, __, __, __, __, __, __},
		{bP, bP, bP, bP, bP, bP, bP, bP},
		{bR, bN, bB, bQ, bK, bB, bN, bR},
	}) {
		t.Errorf("Chessboard not initialized correctly")
	}

	if !reflect.DeepEqual(board.pieces, map[Piece][]Position{
		WhitePawn: {A2, B2, C2, D2, E2, F2, G2, H2},
		WhiteBishop: {C1, F1},
		WhiteRook: {A1, H1},
		WhiteKnight: {B1, G1},
		WhiteQueen: {D1},
		WhiteKing: {E1},
		BlackPawn: {A7, B7, C7, D7, E7, F7, G7, H7},
		BlackBishop: {C8, F8},
		BlackRook: {A8, H8},
		BlackKnight: {B8, G8},
		BlackQueen: {D8},
		BlackKing: {E8},
	}) {
		t.Errorf("Chessboard pieces reference not initialized properly %q", board.pieces)
	}

	if board.CanCastle(WhiteKing) != true {
		t.Errorf("White king should be able to castle when game starts")
	}

	if board.CanCastle(BlackKing) != true {
		t.Errorf("Black king should be able to castle when game starts")
	}

	piece, pieces := board.get(C1)
	if piece != wB {
		t.Errorf("ChessBoard.get() isn't working. Required: %q. Got: %q.", wB, piece)
	}
	if !reflect.DeepEqual(pieces, []Position{C1, F1}) {
		t.Errorf(
			"ChessBoard.get() isn't working. Required: %q. Got: %q.",
			[]Position{C1, F1}, pieces,
		)
	}
}
