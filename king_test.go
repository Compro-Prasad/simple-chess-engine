package main

import (
	"testing"
)


func TestGetKingMoves(t *testing.T) {
	testGetMoves(
		t,
		[8][8]Piece{
			{__, __, __, __, __, __, __, __},
			{__, __, __, __, __, __, __, __},
			{__, bP, __, __, __, bN, __, __},
			{__, wP, __, bK, __, bB, __, __},
			{__, __, __, __, __, wQ, __, __},
			{__, __, wP, bP, __, bQ, __, __},
			{__, __, __, __, __, __, __, __},
			{__, __, __, __, __, __, __, __},
		},
		D4,
		getMovesKing,
		[]Position{E5, C5, E3, C3, E4, C4, D5, D3},
	)
}
