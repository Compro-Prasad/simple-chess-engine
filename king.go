package main


func getMovesKing(board [8][8]Piece, pos Position) []Position {
	var toPos []Position
	var rank, field int8
	rank, field = int8(pos.Rank) - 1, int8(pos.Field) - 1

	color := board[rank][field].GetColor()

	addPos := func (i, j int8) {
		if i < 0 || i > 7 || j < 0 || j > 7 {
			return
		}
		if board[i][j].GetColor() == color {
			return
		}
		position := Position{
			Rank: uint8(i) + 1,
			Field: uint8(j) + 1,
		}
		toPos = append(toPos, position)
	}

	addPos(rank + 1, field + 1)
	addPos(rank + 1, field - 1)

	addPos(rank - 1, field + 1)
	addPos(rank - 1, field - 1)

	addPos(rank, field + 1)
	addPos(rank, field - 1)

	addPos(rank + 1, field)
	addPos(rank - 1, field)

	if color == White && rank == 0 && field == 4 {
		a1piece := board[0][0]
		b1piece := board[0][1]
		c1piece := board[0][2]
		d1piece := board[0][3]

		f1piece := board[0][5]
		g1piece := board[0][6]
		h1piece := board[0][7]

		qSide := b1piece + c1piece + d1piece
		kSide := f1piece + g1piece

		if a1piece == WhiteRook && qSide == 0 {
			addPos(0, 2)
		}
		if h1piece == WhiteRook && kSide == 0 {
			addPos(0, 6)
		}
	} else if color == Black && rank == 7 && field == 4 {
		a8piece := board[7][0]
		b8piece := board[7][1]
		c8piece := board[7][2]
		d8piece := board[7][3]

		f8piece := board[7][5]
		g8piece := board[7][6]
		h8piece := board[7][7]

		qSide := b8piece + c8piece + d8piece
		kSide := f8piece + g8piece

		if a8piece == BlackRook && qSide == 0 {
			addPos(7, 2)
		}
		if h8piece == BlackRook && kSide == 0 {
			addPos(7, 6)
		}
	}

	return toPos
}
