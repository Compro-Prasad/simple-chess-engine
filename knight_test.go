package main

import (
	"testing"
)


func TestGetKnightMoves(t *testing.T) {
	testGetMoves(
		t,
		[8][8]Piece{
			{__, __, __, __, __, __, __, __},
			{__, __, __, __, __, __, __, __},
			{__, bP, __, __, __, bN, __, __},
			{__, wP, __, bN, __, bB, __, __},
			{__, __, __, __, __, wQ, __, __},
			{__, __, wP, bP, __, bQ, __, __},
			{__, __, __, __, __, __, __, __},
			{__, __, __, __, __, __, __, __},
		},
		D4,
		getMovesKnight,
		[]Position{E6, C6, E2, C2, F5, B5},
	)
}
