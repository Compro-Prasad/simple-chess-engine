package main

var GetMoves = map[Piece]func([8][8]Piece, Position) []Position{
	WhiteRook: getMovesRook,
	BlackRook: getMovesRook,

	WhitePawn: getMovesPawn,
	BlackPawn: getMovesPawn,

	WhiteKnight: getMovesKnight,
	BlackKnight: getMovesKnight,

	WhiteKing: getMovesKing,
	BlackKing: getMovesKing,

	WhiteBishop: getMovesBishop,
	BlackBishop: getMovesBishop,

	WhiteQueen: getMovesQueen,
	BlackQueen: getMovesQueen,
}
