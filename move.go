package main

import (
	"fmt"
)

type MoveType uint8

const (
	Normal MoveType = iota

	KingsideCastle
	QueensideCastle
	Capture
	Check
	Checkmate

	CaptureCheck
	CaptureCheckmate

	KingsideCastleCheck
	QueensideCastleCheck

	KingsideCastleCheckmate
	QueensideCastleCheckmate
)


func (piece MoveType) String() string {
	switch piece {
	case Normal: return ""

	case KingsideCastle: return "0-0"
	case QueensideCastle: return "0-0-0"
	case Capture: return "x"
	case Check: return "+"
	case CaptureCheck: return "x+"
	case KingsideCastleCheck: return "0-0+"
	case QueensideCastleCheck: return "0-0-0+"
	case Checkmate: return "#"
	case CaptureCheckmate: return "x#"
	case KingsideCastleCheckmate: return "0-0#"
	case QueensideCastleCheckmate: return "0-0-0#"

	default: panic(fmt.Sprintf("Invalid chess piece reference %v", int(piece)))
	}
}
