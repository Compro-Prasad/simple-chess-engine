HTML_FILE = coverage.html

all:
	go run chessboard.go move.go piece.go position.go rook.go pawn.go getMoves.go knight.go king.go bishop.go queen.go
	go test -coverprofile=coverage.out
	go tool cover -html=coverage.out -o $(HTML_FILE)
