package main

import (
	"fmt"
	"errors"
)

type ChessLog struct {
	piece Piece
	from Position
	to Position
	moveType MoveType
}


func (log ChessLog) String() string {
	return fmt.Sprintf("%v:%v%v%v", log.piece.ShortString(), log.from, log.to, log.moveType)
}


type ChessBoard struct {
	board [8][8]Piece
	pieces map[Piece][]Position
	blackKilledPieces []Piece
	whiteKilledPieces []Piece

	whiteKingMoved bool
	whiteLRookMoved bool
	whiteRRookMoved bool

	blackKingMoved bool
	blackLRookMoved bool
	blackRRookMoved bool

	logs []ChessLog
}


func (board *ChessBoard) CanCastle(piece Piece) bool {
	if piece == WhiteKing {
		return !(board.whiteKingMoved || (board.whiteLRookMoved && board.whiteRRookMoved))
	}
	if piece == BlackKing {
		return !(board.blackKingMoved || (board.blackLRookMoved && board.blackRRookMoved))
	}
	return false
}


func (board *ChessBoard) PrintInfo() {
	fmt.Println("The Board:")
	for i := 0; i < 8; i++ {
		fmt.Print("\t")
		for j := 0; j < 8; j++ {
			fmt.Print(board.board[i][j].ShortString() + " ")
		}
		fmt.Println()
	}
	fmt.Println("Piece Positions:")
	for key := range board.pieces {
		fmt.Println("\t", key, "at", board.pieces[key])
	}
	fmt.Println("Black killed pieces:")
	for i := range board.blackKilledPieces {
		fmt.Println("\t", board.blackKilledPieces[i])
	}
	fmt.Println("White killed pieces:")
	for i := range board.whiteKilledPieces {
		fmt.Println("\t", board.whiteKilledPieces[i])
	}
	fmt.Println("Game Log:")
	for i := range board.logs {
		fmt.Println("\t", board.logs[i])
	}
	fmt.Println("White can castle:", board.CanCastle(WhiteKing))
	fmt.Println("Black can castle:", board.CanCastle(BlackKing))
}


func (board *ChessBoard) get(position Position) (Piece, []Position) {
	piece := board.board[position.Rank - 1][position.Field - 1]
	return piece, board.pieces[piece]
}


func (board *ChessBoard) KillPiece(pos Position) Piece {
	piece, pieces := board.get(pos)
	var color = piece.GetColor()
	for i := range pieces {
		if pieces[i] == pos {
			// Record the killed piece
			var toKill *[]Piece
			if color == White {
				toKill = &board.blackKilledPieces
			} else {
				toKill = &board.whiteKilledPieces
			}
			*toKill = append(*toKill, piece)

			board.removePiece(pos)

			switch (pos) {
			case A1: board.whiteLRookMoved = true
			case H1: board.whiteRRookMoved = true
			case E1: board.whiteKingMoved = true

			case A8: board.blackLRookMoved = true
			case H8: board.blackRRookMoved = true
			case E8: board.blackKingMoved = true
			}

			return piece
		}
	}
	return NoPiece
}


func (board *ChessBoard) removePiece(pos Position) error {
	piece, pieces := board.get(pos)
	if piece == NoPiece {
		return errors.New("No piece at position " + pos.String())
	}
	last := len(pieces) - 1
	for i := range pieces {
		if pieces[i] == pos {
			// Remove from pieces list
			pieces[last], pieces[i] = pieces[i], pieces[last]
			board.pieces[piece] = pieces[:last]
			// Remove from board
			board.board[pos.Rank - 1][pos.Field - 1] = NoPiece
			return nil
		}
	}
	return errors.New("Position " + pos.String() + " is invalid")
}


func (board *ChessBoard) addPiece(pos Position, piece Piece) error {
	if piece == NoPiece {
		return errors.New("No piece to put")
	}
	old_piece, _ := board.get(pos)
	if old_piece != NoPiece {
		return errors.New(fmt.Sprintf("Found %v at %v. Remove it first.",
			old_piece.String(),
			pos.String()))
	}
	board.board[pos.Rank - 1][pos.Field - 1] = piece
	board.pieces[piece] = append(board.pieces[piece], pos)
	return nil
}


func (board *ChessBoard) movePiece(fromPos Position, toPos Position) error {
	fromPosPiece, pieces := board.get(fromPos)
	toPosPiece, _ := board.get(toPos)
	if fromPosPiece == NoPiece {
		return errors.New("No piece to move")
	}
	if toPosPiece != NoPiece {
		return errors.New("Kill the piece first at toPos")
	}
	for i := range pieces {
		if pieces[i] != fromPos {
			continue
		}
		pieces[i] = toPos  // Update in piece list

		// Update the board
		board.board[fromPos.Rank - 1][fromPos.Field - 1] = NoPiece
		board.board[toPos.Rank - 1][toPos.Field - 1] = fromPosPiece
		switch (fromPos) {
		case A1: board.whiteLRookMoved = true
		case H1: board.whiteRRookMoved = true
		case E1: board.whiteKingMoved = true

		case A8: board.blackLRookMoved = true
		case H8: board.blackRRookMoved = true
		case E8: board.blackKingMoved = true
		}
		return nil
	}
	return errors.New("fromPos not found")
}


func (board *ChessBoard) MovePiece(fromPos Position, toPos Position) error {
	var log ChessLog
	fromPosPiece, _ := board.get(fromPos)
	fromPosPieceColor := fromPosPiece.GetColor()
	toPosPiece, _ := board.get(toPos)
	if fromPosPieceColor == Empty {
		return errors.New("No piece selected")
	} else if fromPosPieceColor == toPosPiece.GetColor() {
		return errors.New("Cannot kill same team")
	}

	if board.KillPiece(toPos) != NoPiece {
		log.moveType = Capture
	}

	err := board.movePiece(fromPos, toPos)

	if err == nil {
		log.from = fromPos
		log.to = toPos
		log.piece = fromPosPiece
		board.logs = append(board.logs, log)
	}

	return err
}


func (board *ChessBoard) RemovePieces() {
	if board.pieces == nil {
		// Initialize map
		board.pieces = make(map[Piece][]Position)
	}

	// Delete location of old pieces
	for key := range board.pieces {
		delete(board.pieces, key)
	}

	// Remove pieces from board
	for i := 0; i < 8; i++ {
		for j := 0; j < 8; j++ {
			board.board[i][j] = NoPiece
		}
	}
}


func (board *ChessBoard) Initialize() {
	board.RemovePieces()
	board.addPiece(A1, WhiteRook)
	board.addPiece(B1, WhiteKnight)
	board.addPiece(C1, WhiteBishop)
	board.addPiece(D1, WhiteQueen)
	board.addPiece(E1, WhiteKing)
	board.addPiece(F1, WhiteBishop)
	board.addPiece(G1, WhiteKnight)
	board.addPiece(H1, WhiteRook)

	board.addPiece(A2, WhitePawn)
	board.addPiece(B2, WhitePawn)
	board.addPiece(C2, WhitePawn)
	board.addPiece(D2, WhitePawn)
	board.addPiece(E2, WhitePawn)
	board.addPiece(F2, WhitePawn)
	board.addPiece(G2, WhitePawn)
	board.addPiece(H2, WhitePawn)

	board.addPiece(A8, BlackRook)
	board.addPiece(B8, BlackKnight)
	board.addPiece(C8, BlackBishop)
	board.addPiece(D8, BlackQueen)
	board.addPiece(E8, BlackKing)
	board.addPiece(F8, BlackBishop)
	board.addPiece(G8, BlackKnight)
	board.addPiece(H8, BlackRook)

	board.addPiece(A7, BlackPawn)
	board.addPiece(B7, BlackPawn)
	board.addPiece(C7, BlackPawn)
	board.addPiece(D7, BlackPawn)
	board.addPiece(E7, BlackPawn)
	board.addPiece(F7, BlackPawn)
	board.addPiece(G7, BlackPawn)
	board.addPiece(H7, BlackPawn)
}


func main() {
	var board ChessBoard
	board.Initialize()

	fmt.Println(board.MovePiece(A1, C3))

	fmt.Println("King moves:", GetMoves[WhiteKing](board.board, A4))

	fmt.Println(board.MovePiece(C2, C3))

	fmt.Println(board.MovePiece(C7, C5))
	fmt.Println(board.MovePiece(C3, C6))
	fmt.Println(GetMoves[WhitePawn](board.board, C6))

	fmt.Println(board.MovePiece(A1, A3))
	fmt.Println(board.MovePiece(A8, H1))

	board.PrintInfo()
}
