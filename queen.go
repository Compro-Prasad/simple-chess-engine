package main


func getMovesQueen(board [8][8]Piece, pos Position) []Position {
	return append(getMovesBishop(board, pos), getMovesRook(board, pos)...)
}
