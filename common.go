package main

import (
	"testing"
)


func Find(slice []Position, val Position) int {
	for i, item := range slice {
		if item == val {
			return i
		}
	}
	return -1
}


func testGetMoves(
	t *testing.T,
	board [8][8]Piece,
	position Position,
	getMovesFunc func ([8][8]Piece, Position) []Position,
	moves []Position,
) {
	testMoves := getMovesFunc(board, position)
	validMoves := []Position{}
	invalidMoves := []Position{}
	for _, move := range(testMoves) {
		i := Find(moves, move)
		if i >= 0 {
			validMoves = append(validMoves, move)
			moves[i], moves[len(moves)-1] = moves[len(moves)-1], moves[i]
			moves = moves[:len(moves)-1]
		} else {
			invalidMoves = append(invalidMoves, move)
		}
	}
	if len(invalidMoves) > 0 {
		t.Errorf("Invalid moves: %q", invalidMoves)
	} else if len(moves) > 0 {
		t.Errorf("Unpredicted moves: %q", moves)
	}
}


const (
	__ = NoPiece

	wR = WhiteRook
	wB = WhiteBishop
	wK = WhiteKing
	wQ = WhiteQueen
	wN = WhiteKnight
	wP = WhitePawn

	bR = BlackRook
	bB = BlackBishop
	bK = BlackKing
	bQ = BlackQueen
	bN = BlackKnight
	bP = BlackPawn
)
