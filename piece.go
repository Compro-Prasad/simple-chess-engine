package main

import (
	"fmt"
)


type Piece uint8
type PieceType uint8


const (
	White PieceType = iota
	Black
	Empty
)


const (
	NoPiece Piece = iota

	WhitePawn
	WhiteBishop
	WhiteRook
	WhiteKnight
	WhiteQueen
	WhiteKing

	_
	_

	BlackPawn
	BlackBishop
	BlackRook
	BlackKnight
	BlackQueen
	BlackKing
)


func (color PieceType) String() string {
	switch color {
	case Empty: return "Empty"

	case White: return "White"
	case Black: return "Black"
	default: panic("Invalid PieceType/Color")
	}
}


func (piece Piece) ShortString(params ...string) string {
	var nopiece = "  "
	if (len(params) > 0) {
		nopiece = params[0]
	}
	switch piece {
	case NoPiece: return nopiece

	case WhitePawn: return "wP"
	case WhiteBishop: return "wB"
	case WhiteRook: return "wR"
	case WhiteKnight: return "wN"
	case WhiteQueen: return "wQ"
	case WhiteKing: return "wK"

	case BlackPawn: return "bP"
	case BlackBishop: return "bB"
	case BlackRook: return "bR"
	case BlackKnight: return "bN"
	case BlackQueen: return "bQ"
	case BlackKing: return "bK"
	default: panic(fmt.Sprintf("Invalid chess piece reference %v", int(piece)))
	}
}


func (piece Piece) String() string {
	switch piece {
	case NoPiece: return "NoPiece"

	case WhitePawn: return "WhitePawn"
	case WhiteBishop: return "WhiteBishop"
	case WhiteRook: return "WhiteRook"
	case WhiteKnight: return "WhiteKnight"
	case WhiteQueen: return "WhiteQueen"
	case WhiteKing: return "WhiteKing"

	case BlackPawn: return "BlackPawn"
	case BlackBishop: return "BlackBishop"
	case BlackRook: return "BlackRook"
	case BlackKnight: return "BlackKnight"
	case BlackQueen: return "BlackQueen"
	case BlackKing: return "BlackKing"
	default: panic(fmt.Sprintf("Invalid chess piece reference %v", int(piece)))
	}
}


func (piece Piece) GetOpponentColor() PieceType {
	color := piece.GetColor()
	if color == White {
		return Black
	} else if color == Black {
		return White
	} else {
		panic("No piece selected")
	}
}


func (color PieceType) GetOpponentColor() PieceType {
	if color == White {
		return Black
	} else if color == Black {
		return White
	} else {
		panic("No piece selected")
	}
}


func (piece Piece) GetColor() PieceType {
	if piece == NoPiece {
		return Empty
	} else {
		return PieceType(piece >> 3)
	}
}
